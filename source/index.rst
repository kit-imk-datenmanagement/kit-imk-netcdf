====================================================
Binding Regulations for Storing Data as netCDF Files
====================================================


.. only:: html

      .. authorlist::
            :all:
            :affiliations:


.. toctree::
   :maxdepth: 2
   :caption: Contents

   intro
   general
   global_attributes
   variable_attributes


.. toctree::
   :maxdepth: 2
   :caption: Appendix

   coordinate_examples
   summary_tables


How to cite this document
-------------------------

.. card:: Please do cite this documentation!

   .. tab-set::

      .. tab-item:: APA

         .. citation-info::
            :format: apalike

      .. tab-item:: BibTex

         .. citation-info::
            :format: bibtex

      .. tab-item:: RIS

         .. citation-info::
            :format: ris

      .. tab-item:: Endnote

         .. citation-info::
            :format: endnote

      .. tab-item:: CFF

         .. citation-info::
            :format: cff


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
