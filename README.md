# kit-netcdf-en

Binding Regulations for Storing Data as netCDF Files

## Installation

If you want to build and modify this document locally, you need to install
sphinx.

Here are the steps that you need to build this documentation.

1. Install python on your computer, from source or via [miniconda](https://conda.io/en/latest/miniconda.html)
2. clone this repository via `git clone https://gitlab.hzdr.de/hcdc/hereon-netcdf/hereon-netcdf-en.git`
3. `cd` into the directory that you cloned and run `pip install -r requirements.txt`
5. make your changes in the `*.rst` files, e.g. in [`index.rst`](source/index.rst)
6. run `make html` on linux or `make.bat html` on Windows to generate the
documentation.
7. open `build/html/index.html` in your local browser.

This document is written in restructured Text with [Sphinx](https://www.sphinx-doc.org/en/master/).
If you want to learn more about it, please have a look at the
examples in the [Sphinx documentation](https://www.sphinx-doc.org/en/master/).

Note: We highly recommend to use Visual Studio Code with the
[reStructuredText extension](https://marketplace.visualstudio.com/items?itemName=lextudio.restructuredtext)
to enable a live preview of the documentation while writing.

## Contributing

Please see the [`CONTRIBUTING.md`](CONTRIBUTING.md) file about how to edit and
contribute to this documentation.

## Disclaimer

This documentation is published under a CC-BY 4.0 license. Please see the
*LICENSE* file in the source code repository for details.

Copyright (c) 2021, Helmholtz-Zentrum hereon GmbH
